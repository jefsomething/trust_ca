# trust_ca CICD project:

A python based project consisting of:
- REST APIs to sign SSH host and client certificates
- A Jenkins pipeline to 
  - build the container from a Dockerfile 
  - run functional tests using pytest
  - deploy/build using Ansible playbooks.

This project aims at building a home lab trust CA server.
Users and hosts SSH keys are signed through POST request /v1/{user_key|host_key}, passing the public key name and the content: 


  // request example:
    curl --location --request POST 'dubdub:5000/v1/user_key' \
    --header 'Content-Type: application/json' \
    --data-raw '{"key_name": "mm_foo.pub","key_content": "ssh-rsa AAA...tX4j vagrant@foo"}'

  // return result:
    201 CREATED
    {"message": "ssh-rsa-cert-v01@openssh.com AAA...1lQ== vagrant@foo\n"}

  It is intended for home lab: currently any request will be signed.
  User certificates are valid for 15 minutes, server certificates are valid for 24 hours.
  Since result certificates are not stored there is no DB.
