import subprocess
import argparse
import re
import sys


class sign_user_public_key:


    def __init__(self, principals='vagrant', validity="+1d"):

        parser = argparse.ArgumentParser()
        parser.add_argument("-p", "--principals", help="a comma separated list of principals. ex: vagrant,myadmin")
        parser.add_argument("-v", "--validity", help="validity of cert. ex: +1D")
        parser.add_argument("-l", "--location", help="location of key. ex: pub_keys/")
        parser.add_argument("-k", "--pub_key", help="pub key name. ex: mm_foo.pub")
        args = parser.parse_args()

        self.principals = principals
        if args.principals :
            self.principals = args.principals

        self.validity = validity
        if args.validity :
            self.validity = args.validity

        self.pub_key_location = args.location
        self.pub_key = args.pub_key

    def check_input(self):
        try:
            res = "usage: {}: mm_$name.pub".format(sys.argv[0])
            if ( re.search(r'mm_(\S+).pub', self.pub_key)[1] != '' ) :
                res = re.search(r'mm_(\S+).pub', self.pub_key)[1]
            return 0, res
        except:
            return 1, res

    
    def create_user_cert(self):
        if (self.check_input()[0] == 0) :

            return subprocess.run(
                ["ssh-keygen", "-s", "CA", "-I", self.check_input()[1], "-n", self.principals, "-V", self.validity, self.pub_key],
                cwd = self.pub_key_location
                )

if __name__ == '__main__':
    k = sign_user_public_key().create_user_cert()
