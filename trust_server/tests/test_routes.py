import pytest
from flask import current_app
from flask import Flask
from ..handlers.routes import configure_routes


@pytest.fixture
def client():
    app = Flask(__name__)
    configure_routes(app)
    with app.test_client() as client:
        with app.app_context():
            assert current_app.config["ENV"] == "production"
        yield client


def test_sign_user_key_succeeds(client):
    url = '/v1/user_key'
    key_name = "mm_ansible.pub"
    key_content = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC+BRhmlFvyrC+wE2s1GrhEEk/PbIGX4Qhlonq91nW/mbyijPQnl/9LJ17t3a02PVYR3jfrEfIh+Mde5PWl+YUloAKuMFe4aT7wL0bLpNDJixXeZPsYLSTRPrqRntE1sSljU0u54mbkYXT1abCxbSzHT06T29h5OXJtOlHziRsuvNtDyOoKYp03my+hzsJZJtdgQ+krGA4jCPclFrSMvrKMf17R7i1Dj8AAz0o0b2weqWnkhExc6VYrfB+GDsLEYKmksFk4jgzvRXD9xDNZC6V1l1YXOGPvPUuaIrMM1kquOIc7Y1JTCfcTUXmoHgLrD2+3uXbO+znrgA/nSXtBtX4j vagrant@ansible"
    response = client.post(url, json={"key_name": key_name, "key_content": key_content })
    assert response.status_code == 201


def test_sign_host_keysucceeds(client):
    url = '/v1/host_key'
    key_name = "mm_db.pub"
    key_content = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQC95JW8FuR9pNILSGF+4M9nWkG/0bWfgreG2n0Mh7w78OzzpUqPvh16Zg9pxregMpG+Ja4RzEmdbv94FyaeMBRq+94ptOzR5jA5ajsBen4cvKJ/tI7UCMmSdI8xTbfEHYbyLtned7JEAxr5puT2iKm/du0Jz2fnEbCx5yzDzdEhzXdhHBCfTK15UvCTRUFNywRKXBXEzCkcB/40YF4IZtnFe4R7p415/JO7OdJ3sIbXRRarDqIEra6ih/1EvcIKkaIbhoedJJ+VSp/izC372/VAnUkyMMvxETioZhc5FZmfeyVPVaBZ0J8PV59PsCPWzNTwANO87e8igiyIXpZqNHfNhoKugn01Ttg2YJXa6Id6N6730mVkT9eAjMiMBiFJU5VFB+JE06l351/5cafoOScYjOZ4sJWnuEBgdrVoYd0Ldlvibp7ftp9T40iThgvU2R69ENsp6caC/tGnmqrzbTf4E3+HbF7hnGmlYSBmu4YgG+MufesveaPMXGMd11eg8BU= aj@jaja"
    response = client.post(url, json={"key_name": key_name, "key_content": key_content })
    assert response.status_code == 201
