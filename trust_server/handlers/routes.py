from flask import Flask, request
import json
import re
from ..sign import user as sign_user
from ..sign import host as sign_host
import os

def configure_routes(app):

    def write_key(my_file):
        with open(my_file, 'w') as f:
            f.write(request.json['key_content'] + '\n')


    @app.route('/v1/user_key', methods=['POST'])
    def sign_user_key():
        required_fields = ['key_name', 'key_content']
        for field in required_fields :
            if field not in request.json:
                return json.dumps({"message":"bad request"}), 400

        try:
            key_file = '/tmp/pub_keys/' + request.json['key_name']
            write_key(key_file)

            k = sign_user.sign_user_public_key()
            k.principals = 'vagrant'
            k.validity = '+15m'
            k.pub_key_location = '/tmp/pub_keys'
            k.pub_key = request.json['key_name']

            if (k.create_user_cert().returncode == 0) :
                cert_file = re.search(r'(mm_\S+).pub', request.json['key_name'])[1] + '-cert.pub'
                with open('/tmp/pub_keys/' + cert_file, 'r') as f:
                    cert_content = f.read()
                os.remove('/tmp/pub_keys/' + request.json['key_name'])
                os.remove('/tmp/pub_keys/' + cert_file)
                return json.dumps({"message":cert_content}), 201
                
            else :
                return json.dumps({"message":"signing failure"}), 500

        except:
            return json.dumps({"message":Exception}), 500


    @app.route('/v1/host_key', methods=['POST'])
    def sign_host_key():
        required_fields = ['key_name', 'key_content']
        for field in required_fields :
            if field not in request.json:
                return json.dumps({"message":"bad request"}), 400

        try:
            key_file = '/tmp/pub_keys/' + request.json['key_name']
            write_key(key_file)

            k = sign_host.sign_server_public_key()
            k.validity = '+1D'
            k.pub_key_location = '/tmp/pub_keys'
            k.pub_key = request.json['key_name']

            if (k.create_host_cert().returncode == 0) :
                cert_file = re.search(r'(mm_\S+).pub', request.json['key_name'])[1] + '-cert.pub'
                with open('/tmp/pub_keys/' + cert_file, 'r') as f:
                    cert_content = f.read()
                os.remove('/tmp/pub_keys/' + request.json['key_name'])
                os.remove('/tmp/pub_keys/' + cert_file)
                return json.dumps({"message":cert_content}), 201
                
            else :
                return json.dumps({"message":"signing failure"}), 500

        except:
            return json.dumps({"message":Exception}), 500
