FROM python:3.8-buster
RUN pip install pytest flask
RUN apt install openssh-client

RUN useradd -m -s /bin/bash vagrant -u 997
COPY trust_server trust_server

USER vagrant
RUN mkdir /tmp/pub_keys
RUN ssh-keygen -f /tmp/pub_keys/CA -N ""

CMD ["python", "-m", "trust_server.app"]
